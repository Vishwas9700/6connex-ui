import { ChimeService } from "../service/chime";

export const convertCreateMessagePayloadToChannelMessage = async (payload) => {
  const chatMessage = {
    Content: payload.Content,
    MessageId: payload.MessageId,
    Type: payload.Type,
    LastUpdatedTimestamp: new Date(payload.LastUpdatedTimestamp),
    LastEditedTimestamp: payload.LastEditedTimestamp
      ? new Date(payload.LastEditedTimestamp)
      : undefined,
    CreatedTimestamp: new Date(payload.CreatedTimestamp),
    IsSender: payload.Sender.Arn === (await ChimeService.getChimeBearer()),
    Sender: payload.Sender,
    Redacted: payload.Redacted,
    Metadata: payload.Metadata,
  };
  return chatMessage;
};
