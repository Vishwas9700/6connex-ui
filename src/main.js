import Vue from "vue";
import App from "./App.vue";

import { API, Auth } from "aws-amplify";

import "./styles/app.css";
import { AWS_EXPORTS } from "./aws-exports";

import router from "./router";

import store from "./store";

Auth.configure(AWS_EXPORTS);
API.configure(AWS_EXPORTS);

new Vue({
  el: "#app",
  router,
  store,
  render: (h) => h(App),
});
