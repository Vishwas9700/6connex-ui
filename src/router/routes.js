import { AuthGuard, UnAuthGuard } from "../guards/auth.guard";
// import AddUserToChannel from "../components/AddUserToChannel.vue";
// import CreateChatRoom from "../components/CreateChatRoom.vue";
import Home from "../views/Home.vue";
import Login from "../views/Login.vue";

export const routes = [
  {
    path: "/",
    component: Home,
    beforeEnter: AuthGuard,
  },
  {
    path: "/login",
    component: Login,
    name: "LOGIN",
    beforeEnter: UnAuthGuard,
  },
  { path: "/:catchAll(.*)", redirect: "/login" },
];
