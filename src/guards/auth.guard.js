import Auth from "@aws-amplify/auth";

export const AuthGuard = async (from, to, next) => {
  try {
    await Auth.currentAuthenticatedUser();
    next();
  } catch (err) {
    next({ name: "LOGIN" });
  }
};
export const UnAuthGuard = async (from, to, next) => {
  try {
    await Auth.currentAuthenticatedUser();
    next({ name: "HOME" });
  } catch (err) {
    next();
  }
};
