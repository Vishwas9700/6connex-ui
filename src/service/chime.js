import { API_ROUTES } from "../constants/api.constants";
import { APP_INSTANCE_ARN } from "../constants/app.constants";
import { MESSAGE_VARIANT } from "../constants/channel.constants";
import { environment } from "@/environment/environment";
import { getMessageVariantDetails } from "../utils/common";
import API from "@aws-amplify/api";
import Auth from "@aws-amplify/auth";
import {
  ChannelMessagePersistenceType,
  ChannelMode,
} from "@aws-sdk/client-chime";
import {
  ConsoleLogger,
  DefaultMessagingSession,
  LogLevel,
  MessagingSessionConfiguration,
} from "amazon-chime-sdk-js";
import AWS from "aws-sdk";
import AWSChime from "aws-sdk/clients/chime";
import { v4 as uuidv4 } from "uuid";
import { AuthService } from "./auth";

export class ChimeService {
  static chimeClient;
  static awsChime;
  static chimeBearer;
  static async getClient() {
    if (!this.chimeClient) {
      const credentials = await Auth.currentCredentials().then((data) => data);
      this.chimeClient = await new AWSChime({
        region: "us-east-1",
        credentials,
      });
    }
    return this.chimeClient;
  }
  static async sendMessage(channelId, message, metadata, arn = false) {
    const channelArn = arn ? channelId : this.getChannelArn(channelId);
    const path = API_ROUTES.SEND_CHANNEL_MESSAGE;
    const response = await API.post(environment.apiName, path, {
      body: {
        userId: await AuthService.currentUserSub(),
        channelArn,
        messageContent: message,
        persistence: ChannelMessagePersistenceType.PERSISTENT,
        metadata,
      },
    });
    return response;
  }
  static async createChannel(name, privacy) {
    const path = API_ROUTES.CREATE_CHANNEL;
    const response = await API.post(environment.apiName, path, {
      body: {
        name,
        privacy,
        mode: ChannelMode.UNRESTRICTED,
        userId: await AuthService.currentUserSub(),
      },
    });
    return response;
  }

  static async listChannelMemberships(channelId) {
    const client = await this.getClient();
    const channelArn = this.getChannelArn(channelId);
    const response = await client
      .listChannelMemberships({
        ChannelArn: channelArn,
        ChimeBearer: await this.getChimeBearer(),
      })
      .promise();
    return response;
  }
  static async createChannelMembership(userArn, channelId, arn = false) {
    const channelArn = arn ? channelId : this.getChannelArn(channelId);
    const path = API_ROUTES.CREATE_CHANNEL_MEMBERSHIP;
    const response = await API.post(environment.apiName, path, {
      body: {
        channelArn,
        memberArn: userArn,
        userId: await AuthService.currentUserSub(),
      },
    });
    return response;
  }
  static async listChannels() {
    const path = API_ROUTES.LIST_USER_CHANNELS;
    const response = await API.post(environment.apiName, path, {
      body: {
        userId: await AuthService.currentUserSub(),
      },
    });
    return response.ChannelMemberships?.map(
      (channel) => channel.ChannelSummary
    );
  }
  static async listChannelMessages(channelId, arn = false) {
    const userChimeBearer = await this.getChimeBearer();
    const channelArn = arn ? channelId : this.getChannelArn(channelId);
    const { Channel } = await this.channelInfo(channelArn, true);
    let MessageDeletedAt = undefined;
    try {
      MessageDeletedAt = Number(JSON.parse(Channel.Metadata).MessageDeletedAt);
      // eslint-disable-next-line no-empty
    } catch (err) {}
    const path = API_ROUTES.LIST_CHANNEL_MESSAGE;
    const response = await API.post(environment.apiName, path, {
      body: {
        channelArn,
        userId: await AuthService.currentUserSub(),
        notBefore: MessageDeletedAt,
      },
    });
    response.ChannelMessages = response.ChannelMessages ?? [];
    const result = response.ChannelMessages.map((message) => {
      return {
        Sender: message.Sender,
        Content: message.Content,
        CreatedTimestamp: message.CreatedTimestamp,
        LastUpdatedTimestamp: message.LastUpdatedTimestamp,
        MessageId: message.MessageId,
        Metadata: message.Metadata,
        Redacted: message.Redacted,
        IsSender: message.Sender?.Arn === userChimeBearer,
      };
    }).reduce(
      (acc, cur) => {
        if (!cur.MessageId) return acc;
        const messageVariantDetails = getMessageVariantDetails(cur);
        if (
          messageVariantDetails.AppMessageVariant === MESSAGE_VARIANT.STANDARD
        ) {
          acc.standard.push(cur);
        } else if (
          messageVariantDetails.AppMessageVariant ===
          MESSAGE_VARIANT.THREAD_MESSAGE
        ) {
          const replyMessageId = messageVariantDetails.ReplyMessageId;
          (acc.threadMessages[replyMessageId] =
            acc.threadMessages[replyMessageId] ?? []).push(cur);
        }
        return acc;
      },
      {
        threadMessages: {},
        standard: [],
      }
    );
    console.log(result);
    return result;
  }
  static async channelInfo(channelId, arn = false) {
    const channelArn = arn ? channelId : this.getChannelArn(channelId);
    const path = API_ROUTES.DESCRIBE_CHANNEL;
    const response = await API.post(environment.apiName, path, {
      body: {
        channelArn,
        userId: await AuthService.currentUserSub(),
      },
    });
    return response;
  }
  static async listUsers() {
    const client = await this.getClient();
    const response = await client
      .listAppInstanceUsers({
        AppInstanceArn: APP_INSTANCE_ARN,
      })
      .promise();
    return response.AppInstanceUsers;
  }
  static getChannelArn(id) {
    return `${APP_INSTANCE_ARN}/channel/${id}`;
  }
  static async getChimeBearer() {
    if (!this.chimeBearer) {
      const userSub = await AuthService.currentUserSub();
      this.chimeBearer = `${APP_INSTANCE_ARN}/user/${userSub}`;
    }
    return this.chimeBearer;
  }
  static async getMessagingSessionEndpoint() {
    const path = API_ROUTES.GET_MESSAGING_SESSION_ENDPOINT;
    const response = await API.post(environment.apiName, path, {
      body: {
        userId: await AuthService.currentUserSub(),
      },
    });
    return response;
  }
  static async getSession() {
    const chimeBearer = await this.getChimeBearer();
    const logger = new ConsoleLogger("SDK Chat Demo", LogLevel.INFO);
    const endpoint = await this.getMessagingSessionEndpoint();
    if (!endpoint?.Endpoint?.Url) return;
    const sessionConfig = new MessagingSessionConfiguration(
      chimeBearer,
      uuidv4(),
      endpoint.Endpoint.Url,
      await this.getClient(),
      AWS
    );
    const session = new DefaultMessagingSession(sessionConfig, logger);
    return session;
  }

  static async getDynamoDbClient() {
    const client = await this.getClient();
    AWS.config.update({
      region: "us-east-1",
      credentials: client.config.credentials,
    });
    const docClient = new AWS.DynamoDB.DocumentClient();
    return docClient;
  }

  static async sendThreadMessage(
    channelId,
    messageId,
    messageContent,
    arn = false
  ) {
    const channelArn = arn ? channelId : this.getChannelArn(channelId);
    const path = API_ROUTES.SEND_THREAD_MESSAGE;
    const response = await API.post(environment.apiName, path, {
      body: {
        userId: await AuthService.currentUserSub(),
        messageContent,
        channelArn,
        persistence: ChannelMessagePersistenceType.PERSISTENT,
        replyMessageId: messageId,
      },
    });
    return response;
  }

  static async deleteChannelMessages(channelId, arn = false) {
    // const chimeClient = await this.getClient();
    const channelArn = arn ? channelId : this.getChannelArn(channelId);
    const { Channel } = await this.channelInfo(channelArn, true);
    if (
      !Channel?.ChannelArn ||
      !Channel.Mode ||
      !Channel.Name ||
      !Channel.CreatedBy?.Arn
    )
      return;
    const metadata = JSON.parse(Channel.Metadata ?? "{}");
    metadata["MessageDeletedAt"] = new Date().valueOf();
    const path = API_ROUTES.UPDATE_CHANNEL;
    const response = await API.post(environment.apiName, path, {
      body: {
        userId: await AuthService.currentUserSub(),
        channelArn,
        name: Channel.Name,
        mode: Channel.Mode,
        metadata: JSON.stringify(metadata),
      },
    });
    return response;
  }
}
