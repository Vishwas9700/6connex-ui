import { Auth } from "aws-amplify";
export class AuthService {
  static async login(username, password) {
    const user = await Auth.signIn(username, password);
    return user;
  }
  static async changePassword(user, newPassword) {
    const response = await Auth.completeNewPassword(user, newPassword);
    return response;
  }
  static async currentUserSub() {
    const response = await Auth.currentUserInfo();
    return response.attributes.sub;
  }
  static currentUser() {
    return Auth.currentUserInfo();
  }
}
