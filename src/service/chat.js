import { ChimeService } from "./chime";
import { v4 as uuidv4 } from "uuid";

export class ChatService {
  static messageCallbacks = {};
  static addListener(fn) {
    const key = uuidv4();
    this.messageCallbacks[key] = fn;
    return key;
  }
  static async listen() {
    const messageObserver = {
      messagingSessionDidStart: () => {},
      messagingSessionDidStartConnecting: () => {},
      messagingSessionDidStop: () => {},
      messagingSessionDidReceiveMessage: (message) =>
        this.publishMessage(message),
    };
    const session = await ChimeService.getSession();
    session?.addObserver(messageObserver);
    session?.start();
  }
  static publishMessage(message) {
    Object.values(this.messageCallbacks).forEach((cb) => cb?.(message));
  }
  static removeListener(key) {
    delete this.messageCallbacks[key];
  }
  static async getChannelsInRoom(roomId) {
    console.log(roomId);
    const response = await ChimeService.listChannels();
    return response;
  }
}
