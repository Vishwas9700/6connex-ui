import Vue from "vue";

export const ChannelInfo = {
  state: {},
  actions: {
    setChannelInfo({ commit }, { channelId, channelArn, channelName }) {
      commit("setChannelInfo", { channelId, channelArn, channelName });
    },
  },
  mutations: {
    setChannelInfo(state, { channelId, channelArn, channelName }) {
      Vue.set(state, channelId, { channelArn, channelName });
    },
  },
};
