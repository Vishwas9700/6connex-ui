import Vue from "vue";

export const ThreadMessage = {
  state: {},
  actions: {
    addThread({ commit }, { messageId, data }) {
      commit("addThread", { messageId, data });
    },
    addThreadMessage({ commit }, { messageId, message }) {
      commit("addThreadMessage", { messageId, message });
    },
  },
  mutations: {
    addThread(state, { messageId, data }) {
      state[messageId] = data ?? [];
    },
    addThreadMessage(state, { messageId, message }) {
      const isMessageArray = Array.isArray(message);
      if (!state[messageId]) {
        Vue.set(state, messageId, isMessageArray ? [...message] : [message]);
      } else {
        if (Array.isArray(message)) {
          state[messageId].push(...message);
        } else {
          state[messageId].push(message);
        }
      }
    },
  },
};
