import Vue from "vue";

export const ChannelMessage = {
  state: {},
  actions: {
    setChannelMessage({ commit }, { channelId, messages }) {
      commit("setChannelMessage", { channelId, messages });
    },
    addChannelMessage({ commit }, { channelId, message }) {
      commit("addChannelMessage", { channelId, message });
    },
    deleteChannelMessage({ commit }, { channelId, deletedAt }) {
      commit("deleteChannelMessage", { channelId, deletedAt });
    },
  },
  mutations: {
    deleteChannelMessage(state, { channelId, deletedAt }) {
      const channelMessages = state[channelId] ?? [];
      const filteredResult = channelMessages.filter(
        (message) =>
          message.CreatedTimestamp &&
          message.CreatedTimestamp.valueOf() > deletedAt
      );
      Vue.set(state, channelId, filteredResult);
    },
    setChannelMessage(state, { channelId, messages }) {
      Vue.set(state, channelId, messages);
    },
    addChannelMessage(state, { channelId, message }) {
      const isMessageArray = Array.isArray(message);
      if (!state[channelId]) {
        Vue.set(state, channelId, isMessageArray ? [...message] : [message]);
      } else {
        if (isMessageArray) {
          state[channelId].push(...message);
        } else {
          state[channelId].push(message);
        }
      }
    },
  },
};
