import Vue from "vue";
import Vuex from "vuex";
import { ChannelInfo } from "./channelInfo";
import { ChannelMessage } from "./channelMessages";
import { ThreadMessage } from "./threadMessages";

Vue.use(Vuex);

export default new Vuex.Store({
  state: {},
  mutations: {},
  actions: {},
  modules: {
    ChannelInfo,
    ChannelMessage,
    ThreadMessage,
  },
});
