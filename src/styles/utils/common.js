import { MESSAGE_VARIANT } from "@/constants/channel.constants";

export const convertArnToChannelId = (channelArn) => {
  return channelArn.split("/").slice(3, 4)[0];
};

export const getMessageVariantDetails = (
  message 
) => {
  const metadata = message.Metadata;
  const { AppMessageVariant, ReplyMessageId } = JSON.parse(metadata ?? "{}");
  if (AppMessageVariant === MESSAGE_VARIANT.THREAD_MESSAGE)
    return {
      AppMessageVariant: MESSAGE_VARIANT.THREAD_MESSAGE,
      ReplyMessageId: ReplyMessageId,
    };
  return { AppMessageVariant: MESSAGE_VARIANT.STANDARD };
};
